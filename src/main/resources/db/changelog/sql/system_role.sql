CREATE TABLE system_role
(
    system_role varchar(30) PRIMARY KEY,
	description varchar(100)
);

grant select on system_role to app;
grant select, insert, update, delete on system_role to admin;

insert into system_role values ('admin', 'Администратор');
insert into system_role values ('user', 'Внутренний пользователь системы');
insert into system_role values ('customer', 'Заказчик');