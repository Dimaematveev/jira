package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mephi.jira.dao.ProjectRoleDao;
import ru.mephi.jira.model.ProjectRole;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectRoleController {
    private final ProjectRoleDao projectRoleDao;

    @GetMapping(value = "/projectRole/get", produces = APPLICATION_JSON_VALUE)
    public List<ProjectRole> getProjectRoles() {
        return projectRoleDao.fetchAll();
    }

}
