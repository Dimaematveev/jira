package ru.mephi.jira.controller.ExceptionHandler;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Order;
import org.springframework.core.Ordered;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.mephi.jira.exeption.NotEnoughRightException;
import ru.mephi.jira.model.forController.ErrorResponse;

import java.sql.SQLException;

@ControllerAdvice
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {NotEnoughRightException.class})
    private ResponseEntity<Object> handleError() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ErrorResponse.builder()
                        .errorCode("VALIDATION_ERROR")
                        .errorMessage("USER_DONT_HAVE_RIGHTS")
                        .build());
    }

    @ExceptionHandler(value = {DataAccessException.class})
    private ResponseEntity<Object> handleDbError(DataAccessException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ErrorResponse.builder()
                        .errorCode("DB_ERROR")
                        .errorMessage(e.getMessage())
                        .build());
    }
}
