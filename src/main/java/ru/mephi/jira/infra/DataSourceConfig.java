package ru.mephi.jira.infra;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    public  static final String JIRA = "JIRA_DATASOURCE";

    @Bean
    @Primary
    @ConfigurationProperties("jira.datasource")
    public DataSourceProperties dataSourceProperties(){
        return new DataSourceProperties();
    }

    @Bean(JIRA)
    public DataSource dataSource(){
        return dataSourceProperties().initializeDataSourceBuilder().build();
    }
}
