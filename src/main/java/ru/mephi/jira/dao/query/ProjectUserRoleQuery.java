package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProjectUserRoleQuery {

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('project_user_role_id_seq')"),

    GET_PROJECT_ROLE_BY_PROJECT_USER_ID("Получение project_role_id по project_user_id",
            "select pr.project_role_id, pr.project_role, pr.description " +
                    "from project_user_role pur " +
                    "join project_role pr on pur.project_role_id=pr.project_role_id " +
                    "where pur.project_user_id=:project_user_id"),

    INSERT_PROJECT_USER_ROLE("Добавление роли на проект",
            "insert into project_user_role (id, project_user_id, project_role_id) " +
                    "values (:id, :project_user_id, :project_role_id)");

    private final String description;
    private final String query;
}
