package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProjectRoleQuery {
    GET_PROJECT_ROLES("Получение всех возможных ролей",
            "select * from project_role order by project_role_id"),

    GET_PROJECT_ROLE_ID_BY_NAME("Получение id роли по имени",
                              "select project_role_id from project_role where project_role=:project_role");

    private final String description;
    private final String query;
}
