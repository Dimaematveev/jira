package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum SystemUserQuery {
    GET_SYSTEMUSERS("Получение всех systemUser",
            "select * from system_user order by user_id"),

    GET_SYSTEMUSER_BY_ID("Получение systemUser по id",
            "select * from system_user where user_id=:user_id"),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('system_user_user_id_seq')"),

    INSERT_SYSTEMUSER("Создание systemUser",
            "insert into system_user (user_id, name, login, system_role) values (:user_id, :name, :login, :system_role)");

    private final String description;
    private final String query;
}
