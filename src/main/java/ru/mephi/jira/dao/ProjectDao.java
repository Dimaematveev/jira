package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.Project;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.SystemUser;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.ProjectQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class ProjectDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProjectDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Project> fetchAll() {
        return namedParameterJdbcTemplate.query(GET_PROJECTS.getQuery(), new ProjectRowMapper());
    }

    public void updateFullDescription(Integer projectId, String fullDescription) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_id", projectId)
                .addValue("full_description", fullDescription);
        namedParameterJdbcTemplate.update(UPDATE_FULL_DESCRIPTION_TO_PROJECT.getQuery(), params);
    }

    public void updateOwnerId(Integer projectId, Integer ownerId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_id", projectId)
                .addValue("owner_id", ownerId);
        namedParameterJdbcTemplate.update(UPDATE_OWNER_TO_PROJECT.getQuery(), params);
    }

    public Integer insertProject(Project project) {
        Integer projectId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_id", projectId)
                .addValue("project_name", project.getName())
                .addValue("short_description", project.getShortDescription());
        namedParameterJdbcTemplate.update(INSERT_PROJECT.getQuery(), params);
        return projectId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

    public static class ProjectRowMapper implements RowMapper<Project> {
        @Override
        public Project mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Project(
                    rs.getInt("project_id"),
                    rs.getString("project_name"),
                    rs.getString("short_description"),
                    rs.getString("full_description") != null ? rs.getString("full_description") : null,
                    rs.getTimestamp("create_date").toLocalDateTime(),
                    rs.getString("owner_id") != null ? new SystemUser(rs.getInt("owner_id")) : null
            );
        }
    }
}
