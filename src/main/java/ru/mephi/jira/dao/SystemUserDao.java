package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.SystemUser;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.SystemUserQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class SystemUserDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public SystemUserDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<SystemUser> fetchAll() {
        return namedParameterJdbcTemplate.query(GET_SYSTEMUSERS.getQuery(), new SystemUserRowMapper());
    }

    public SystemUser fetchByUserId(Integer userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("user_id", userId);
        return namedParameterJdbcTemplate.queryForObject(GET_SYSTEMUSER_BY_ID.getQuery(), params, new SystemUserRowMapper());
    }

    public Integer insertSystemUser(SystemUser systemUser) {
        Integer userId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("user_id", userId)
                .addValue("name", systemUser.getName())
                .addValue("login", systemUser.getLogin())
                .addValue("system_role", systemUser.getSystemRole());
        namedParameterJdbcTemplate.update(INSERT_SYSTEMUSER.getQuery(), params);
        return userId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

    public static class SystemUserRowMapper implements RowMapper<SystemUser> {
        @Override
        public SystemUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new SystemUser(
                    rs.getInt("user_id"),
                    rs.getString("name"),
                    rs.getString("login"),
                    rs.getString("system_role")
            );
        }
    }
}
