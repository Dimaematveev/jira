package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.SystemUser;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.ProjectUserQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class ProjectUserDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Autowired
    public ProjectUserDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<ProjectUser> getByProjectId(Integer projectId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_id", projectId);
        return namedParameterJdbcTemplate.query(GET_PROJECT_USER_BY_PROJECT_ID.getQuery(), params, (rs, i) -> {
            ProjectUser projectUser = new ProjectUser();
            projectUser.setId(rs.getInt("project_user_id"));
            projectUser.setSystemUser(new SystemUser(rs.getInt("system_user_id")));
            return projectUser;
        });
    }

    public Integer insertProjectUser(Integer systemUserId, Integer projectId) {
        Integer projectUserId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_user_id", projectUserId)
                .addValue("system_user_id", systemUserId)
                .addValue("project_id", projectId);
        namedParameterJdbcTemplate.update(INSERT_PROJECT_USER.getQuery(), params);
        return projectUserId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

}
