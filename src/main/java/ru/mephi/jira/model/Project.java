package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Project {
    private Integer id;
//    private List<Task> tasks;
    private String name;
    private String shortDescription;
    private String fullDescription;
    private LocalDateTime createDate;
    private SystemUser owner; //По спеке тут ProjectUser, но так гораздо лучше
//    private List<ProjectUser> users;

    public Project(String name, String shortDescription) {
        this.name = name;
        this.shortDescription = shortDescription;
    }
}
