package ru.mephi.jira.model;

import lombok.Data;

import java.util.List;

@Data
public class ProjectUser {
    private Integer id;
    private Project project;
    private SystemUser systemUser;
    private List<ProjectRole> projectRoles;
}
