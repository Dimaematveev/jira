package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProjectRole {
    private Integer id;
    private String name;
    private String description;
}
