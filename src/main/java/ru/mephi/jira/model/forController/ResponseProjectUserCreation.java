package ru.mephi.jira.model.forController;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseProjectUserCreation {
    private Integer projectUserId;
}
