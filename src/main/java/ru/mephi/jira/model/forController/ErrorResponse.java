package ru.mephi.jira.model.forController;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ErrorResponse {
    private final String errorCode;
    private final String errorMessage;
}
